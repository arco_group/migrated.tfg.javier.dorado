\chapter{Método de trabajo}
\label{chap:metodo}

\drop{E}{n} este capítulo se describe la metodología de trabajo que se ha utilizado para 
la realización y ejecución de este proyecto.

\section{Metodología de trabajo}
\label{metodologia_de_trabajo}
Para el desarrollo y planificación del proyecto elegimos una metodología ágil porque pensamos
que es la que mejor se adapta a las necesidades del mismo.

Las metodologías tradicionales funcionan muy bien en proyectos dónde el problema está acotado, 
es conocido y la solución está bien definida, y por lo tanto, en este entorno el análisis, diseño
y ejecución del proyecto es muy sencillo. Por el contrario, en una metodología ágil los requisitos
y las soluciones evolucionan a partir de una división del trabajo en módulos o bloques, así se 
minimizan riesgos en los plazos de entrega y también se simplifica más y resulta más sencillo. 
Como al comienzo del planteamiento del proyecto las especificaciones no están acotadas totalmente,
usando esta metodología evitamos riesgos y sobre todo las decisiones tomadas en una  planificación
inicial pueden ser modificadas con el menor impacto posible en el proyecto.

Las metodologías ágiles como vemos se centran en aspectos como flexibilidad para introducir cambios
en el proyecto y tener nuevos requisitos durante el mismo. También se interesan por el factor
humano, el producto final o la colaboración con el cliente, y por todo ello pensamos que una metodología
ágil es la metodología que necesitamos para el desarrollo del proyecto que nos concierne.

Algunas de las metodologías ágiles más usadas son:

\begin{itemize}
\item \textbf{Scrum:} Proporciona una serie de herramientas y roles para que de una forma iterativa 
e incremental, se puedan ver el progreso y los resultados de un proyecto. En un primer momento se sigue
la lista de requisitos del cliente, pero puede ser alterada por el mismo si considera que es mejor
para el desarrollo, pudiendo provocar modificaciones en el orden de prioridad de las tareas. En cada 
iteración el resultado tiene la garantía de funcionar correctamente y ya no puede ser modificado.
\item \textbf{Kanban:} Se basa en que el trabajo en curso debe limitarse y solamente se debería comenzar
con un bloque nuevo, cuando un bloque de trabajo anterior haya sido entregado o ha pasado a otra función
de la cadena.
\item \textbf{Programación Extrema (XP):} Potencia las relaciones interpersonales como clave del éxito
en el desarrollo del software y promueve el trabajo en equipo. El equipo de desarrollo ha de cumplir 
estrictamente con el orden de prioridad de las tareas especificadas por el cliente, aunque las tareas 
pueden ser modificadas durante el desarrollo del proyecto, a pesar de tener un funcionamiento correcto.
Los miembros del equipo de desarrollo trabajan en parejas.
\end{itemize}

En primer lugar nos interesa un trabajo incremental e iterativo, porque al inicio no tenemos bien definida
la solución, ni todos los problemas bajo control. En el momento de plantear requisitos no sabemos si en el 
trascurso del desarrollo aparecerá cualquier factor pueda modificar nuestra lista de requisitos y queremos 
asegurarnos que si los requisitos cambian porque la solución así lo precise, lo hagamos de tal forma que 
el proyecto no note las consecuencias de estos cambios, y mantengamos y garantizemos buenos resultados.

También queremos que no haya riesgos si los plazos de entrega se reducen y sobre todo que el desarrollo posea
la misma calidad a pesar de reducir el tiempo.

Otro tema que concierne al proyecto es que en cada iteración el resultado funcione correctamente y que ya esa
iteración no pueda ser modificada.

Después de estudiar las alternativas y posibilidades que nos ofrecen las distintas metodologías ágiles, 
se ha escogido Scrum para el desarrollo de este proyecto, ya que nos ofrece mucha flexibilidad y a la vez
garantiza buenos resultados, además de adaptarse perfectamente a nuestro desarrollo en concreto. En las 
siguientes secciones, se explican las fases que tiene Scrum, roles y responsabilidades y conceptos y prácticas
que hay en esta metodología. Sin embargo, si el lector ya conoce esta metodología o está familiarizado con
la misma, se recomienda comenzar por la próxima subsección (véase \S\,\ref{roles_desarrollo}), en la que 
se describe cómo se ha llevado a cabo el método de trabajo en nuestro desarrollo.


\subsection{Scrum}
\label{scrum}
Scrum es una metodología ágil y flexible para gestionar el desarrollo software en la que se
aplican de manera regular un conjunto de buenas prácticas para el trabajo colaborativo en equipo, y 
obtener el mejor resultado posible dentro de un proyecto. Las buenas prácticas se apoyan unas a otras
y su selección surge en un estudio de la forma de trabajar de equipos altamente productivos. Esta metodología 
se basa en construir primero la funcionalidad de mayor valor para el cliente y en los principios
de inspección continua, adaptación, auto-gestión e innovación.

Scrum permite que nuestro desarrollo sea \textit{ágil}, porque divide el trabajo en pequeñas unidades funcionales 
llamadas sprints y se puede mantener una política de entregas frecuentes de software que ofrecen una 
visión clara del estado del proceso y permite la introducción de modificaciones. Otra característica de esta 
metodología es que es \textit{simple} y se centra en facilitar el desarrollo rápido, por lo que su complejidad se 
reduce al máximo. Un adjetivo muy propio de esta metodología es que es \textit{flexible} porque permite introducir 
modificaciones "sobre la marcha" para mejorar el proceso, y esto es posible porque el desarrollo se contempla como
un ciclo de iteraciones continuas de desarrollo. Por último destacamos su carścter \textit{colaborativo}, ya que otorga
a los miembros del equipo de desarrollo una gran autonomía y autoorganización de su trabajo.

Analizando estas características de Scrum, vemos que se suele utilizar en entornos complejos, dónde lo importante es obtener
resultados pronto, dónde los requisitos son cambiantes o poco definidos o dónde priman la innovación, competitividad, 
flexibilidad y productividad. También se enfoca en sistemas que contempla numerosas variables como recursos, tiempo,
tecnología y que pueden ser afectadas por algunos factores en cualquier momento del desarrollo.

\subsubsection{Fases de Scrum}
\label{fases}
Podemos distinguir tres fases bien diferenciadas en Scrum:

\begin{itemize}
\item \textbf{Pre-game:} Esta fase se divide en dos subfases.

La primera subfase se conoce como planificación, y en ésta se define el sistema a desarrollar. El cliente presenta al equipo de 
desarrollo el \textit{"Product Backlog List"}, que es la lista de requisitos con prioridad para el desarrollo del proyecto. El equipo se
compromete a llevar a cabo esa lista y se preguntan las dudas que surjan.

En la segunda subfase se hace el diseño de alto nivel del sistema, y consiste en planificar las tareas que se van a realizar
en cada iteración para conseguir satisfacer los requisitos (\textit{Sprint Planning}). Las iteraciones están organizadas según la
prioridad que tienen asignada en el \textit{"Product Backlog List"}, teniendo en cuenta la dependencia entre tareas.

\item \textbf{Development:} Es la fase dónde se lleva a cabo el desarrollo del proyecto. Cuando se define el \textit{Sprint Planning}, se lleva
a cabo el \textit{Sprint}, que es la ejecución de la iteración. Cada día el equipo se reune para sincronizarse (\textit{Scrum daily meeting})
para ver y analizar el estado de la iteración y así poder hacer adaptaciones si fuera necesario. Esta fase es iterativa e incremental y se obtienen
las diferentes versiones y módulos del sistema. Cada iteración está compuesta por cinco fases, y son:

Planificación: identificación de actividades para llevar a cabo la iteración dónde se definen los objetivos de la misma.

Análisis de requisitos: realización de un estudio de los requisitos que se deben cumplir en la iteración.

Diseño: se realizan diagramas o esquemas que ayuden a comprender las acciones a realizar, y se determina la estrategia que se utilizará.

Codificación: se lleva a cabo la implementación de todo lo realizado en las fases anteriores.

Revisión: se comprueba que no existen errores en las acciones realizadas y que se alcanza el objetivo.

Documentación: se lleva a cabo un contenido escrito dónde aparecen diagramas o esquemas del propio desarrollo y sirve para entender el
sistema o para que sea más fáciluna futura modificación.

\item \textbf{Post-game:} Es la fase final tras cumplir los requisitos que se pactaron al principio con el cliente, y se hace una reunión
llamada \textit{Sprint Review}, dónde el cliente comprueba si los requisitos se han cumplido o hay que realizar algún cambio o modificación.
Esta fase abarca la preparación para lanzar la versión y pruebas antes del lanzamiento de la versión.

\end{itemize}

En la Figura \ref{fig:fases_scrum} se muestra un simple esquema a modo de resumen de cada una de las actividades que se llevan a cabo durante
dichas fases.

 \begin{figure}[!h]
\begin{center}
\includegraphics[width=0.8\textwidth]{scrum5.jpg}
\caption{Fases de Scrum ~\cite{webfaseScrum}.}
\label{fig:fases_scrum}
\end{center}
\end{figure}

\subsubsection{Roles y responsabilidades}
\label{roles}
Como destacamos anteriormente (véase \S\,\ref{scrum}), una de las características de Scrum es el trabajo colaborativo, y para ello es 
necesario que exista una comunicación y colaboración entre los distintos miembros del equipo, y también que todo ello exista con el 
cliente. Para que todo esto sea un éxito cada miembro implicado en el desarrollo del proyecto tiene unas responsabilidades y unas tareas
para llevar a cabo. Se pueden distinguir hasta seis roles (Product Owner, Scrum Master, Scrum Team, Customer, User y Management), aunque 
en este apartado describiremos los roles más importantes y las tareas a desempeñar de cada uno:

\begin{itemize}
\item \textbf{Product Owner:} Es la persona responsable de transmitir al equipo de desarrollo la visión del producto que se quiere crear,
aportando la perspectiva del negocio. Representa al resto de interesados en el desarrollo del producto. Tiene la responsabilidad de
definir el conjunto de requerimientos (\textit{Product Backlog}) , de priorizarlos y validarlos. También puede replanificar el proyecto 
al final de cada iteración, nunca durante ésta, según los resultados de iteraciones anteriores en el proyecto.
\item \textbf{Scrum Master:} Es un líder o administrador del equipo de desarrollo y su papel principal consiste en garantizar que el 
equipo de trabajo no tenga impedimentos u obstáculos en el desarrollo y que las tareas se desarrollen de la manera más productiva posible.
Por otra parte es el responsable de asegurar  que el proyecto es realizado de acuerdo a las prácticas establecidas, además de que el 
proyecto evoluciona según lo previsto. 
\item \textbf{Scrum Team:} Es el equipo responsable de desarrollar y entregar el producto y poseen habilidades transversales necesarias
para realizar el trabajo (análisis, diseño, desarrollo, pruebas, documentación, etc.) .Mantiene una organización horizontal en la que 
cada miembro se autogestiona y se organiza libremente en la definición y ejecución de sprints. Es necesaria una buena comunicación entre
los miembros del equipo, así como con el líder para que el proyecto evolucione favorablemente.
\end{itemize}

Además de estos roles principales tenemos los denominados roles auxiliares que no tienen un rol formal y no se involucran
frecuentemente en el proceso Scrum, pero han de ser tomados en cuenta, ya que es importante involucrar en el proceso a
usuarios, expertos del negocio y otros interesados para que participe en la retroalimentación con el fin de revisar y planear
cada sprint. Estos roles son:

\begin{itemize}
\item \textbf{Stakeholders:} Es un conjunto de personas que no forman parte directa en el desarrollo pero que se deben de tener en
cuenta por ser personas interesadas en el mismo, como directores, gerentes, comerciales, proveedores, etc. El Product Owner se encargará de
recoger sus opiniones y sugerencias y decidir si las aplica a la definición del proyecto. Sólo participan directamente durante
las revisiones del sprint.
\item \textbf{Administradores:} Establecen el ambiente para el desarrollo del proyecto.
\end{itemize}

En la Figura \ref{fig:fases_scrum} también se observan algunos de los roles más importantes.

\subsubsection{Conceptos importantes}
\label{conceptos_practicas}
En este apartado vamos a desarrollar conceptos que han salido en los apartados anteriores para que tengamos claro lo que significan
ya que algunos los nombramos en apartados anteriores pero sin explicar. Algunos de estos conceptos son:

\begin{itemize}
\item \textbf{Sprint o Iteración:} Son los períodos temporales cortos y fijos en que dividimos la ejecución del proyecto. La duración
de estos períodos suele ser entre 2 y 4 semanas, aunque esta duración puede ir ajustándose según el ritmo del equipo. Cada iteración
ha de proporcionar un resultado completo, algo potencialmente entregable por si el Product Owner lo solicitase en cualquier momento
para su uso. En estos bloques de tiempo se realizan algunas actividades como reuniones de planificación de sprints, reuniones diarias
y el Sprint Backlog.
\item \textbf{Product Backlog:} Es un documento de alto nivel que contiene la lista de objetivos/requisitos priorizada y representa la 
visión y expectativas del cliente. Los requisitos suelen estar acompañados de estimaciones a grandes rasgos como el valor aportado o
el esfuerzo de desarrollo requerido. Es abierto y sólo lo puede modificar el Product Owner a lo largo de las iteraciones por eso podemos
decir que se encuentra en continua evolución.
\item \textbf{Sprint Backlog:} Es la lista de tareas que el equipo realiza en la reunión de planificación de la iteración (Sprint Planning)
como plan para completar los requisitos seleccionados para la iteración. Las tareas se dividen en horas pero ninguna con una duración
superior a dieciséis horas, no obstante, si la tarea es mayor de dieciséis horas, se dividirá en otras menores.
\item \textbf{Sprint Planning:} Es la llamada reunión de planificación de la iteración y se divide en dos partes. En una primera parte se 
definen los objetivos y funcionalidades a desarrollar en el siguiente Sprint, y el equipo se reúne con el cliente para seleccionar los 
objetivos prioritarios y preguntarle dudas al cliente. En la segunda parte se reúne el equipo para decidir el plan o la táctica para abordar
cada una de las tareas del siguiente Sprint.
\item \textbf{Estimación de esfuerzo:} La realiza el Product Owner junto al equipo de desarrollo y se pretende hacer una estimación de 
esfuerzo de cada uno de los elementos del Product Backlog. Si se tiene mucha información del proceso a estimar, la estimación será más 
prescisa que si se posee poca información.
\item \textbf{Daily Scrum:} Son reuniones diarias de sincronización del equipo, y su objetivo es facilitar la transferencia de información 
y colaboración entre los miembros del equipo para el aumento de la productividad y poderse ayudar entre ellos. Deben de ser cortas y no tener
una duración de más de quince minutos.
\item \textbf{Sprint Review:} Son reuniones que se realizan al final de cada Sprint en la que Scrum Master y Scrum Team presentan los 
resultados a los interesados. No debe durar más de cuatro horas.
\end{itemize}

\subsection{Roles de Scrum asignadas en nuestro proyecto}
\label{roles_desarrollo}
A continuación explicamos cómo repartimos los roles y las responsabilidades en nuestro proyecto y aunque no tenemos todos los roles, ni un equipo
de desarrollo, hemos asignado los roles más importantes de esta forma \ref{fig:roles_arthrikin}:

\begin{itemize}
\item \textbf{Product Owner:} Se puede decir que el director de proyecto marca los objetivos del proyecto, destacando los requisitos que se
deben de cumplir, por lo tanto este rol lo desempeñaría él.
\item \textbf{Scrum Master:} Este rol lo desempeña el director de proyecto, ya que en todo momento es el que toma el papel de líder y va 
coordinando el proyecto, también se encarga de que todo vaya correctamente, va resolviendo problemas que se van encontrando, etc.
\item \textbf{Scrum Team:} Como mencionamos anteriormente en este proyecto el único miembro del equipo es el alumno, por tanto es que tendrá
este rol durante el desarrollo.
\end{itemize}

 \begin{figure}[!h]
\begin{center}
\includegraphics[width=0.9\textwidth]{roles.png}
\caption{Roles Arthrikin.}
\label{fig:roles_arthrikin}
\end{center}
\end{figure}


\subsection{Fases de Scrum asignadas en nuestro proyecto}
\label{fases_desarrollo}
En esta sección se describen las fases de Scrum aplicadas en nuestro proyecto. Como se explicó en la sección \ref{scrum}, en la primera 
fase se elaborará la lista de requisitos, después, en la segunda fase, comenzarán las iteraciones para el desarrollo, y por último, se realizarán
las pruebas y revisiones correspondientes para asegurarnos que funciona correctamente.

\subsubsection{Pre-Game}
La primera fase tiene como objetivos principales realizar la lista de requisitos o "Product Backlog", basándonos en los objetivos anteriormente
expuestos (véase \S\,\ref{objetivos}). Otro de los objetivos a tener en cuenta en esta fase es la de la planificación de las tareas en
cada iteración a partir de los requisitos para tenerlo todo en cuenta en el desarrollo.

Se nos presenta la lista de requisitos priorizada, por lo que seguiremos el orden de prioridad expuesto a continuación en el Product Backlog List:

\begin{itemize}
\item \textbf{Análisis del estado del arte:} Necesitamos tener una visión general sobre los temas y las herramientas de las que disponemos
en la actualidad para el desarrollo de nuestro proyecto. Queremos conocer detalles sobre el tema a tratar que es la \acs{AR}, también
conocer el dispositivo que nos capture ejercicios y ciertas herramientas o conceptos que nos ayuden a completar el desarrollo.
\item \textbf{Análisis y elección de librería para el desarrollo:} Debemos analizar y encontrar las librerías que existen para realizar
nuestro proyecto, y seleccionar la que mejor se adapte y de soporte a nuestra aplicación de una mejor forma.
\item \textbf{Identificación y análisis de los datos recogidos por el dispositivo:} Para poder cumplir este objetivo, tenemos que tener 
claros numerosos conceptos y claves del dispositivo y de la librería y sobre todo adquirir habilidades técnicas para su control.
\item \textbf{Análisis de los datos recogidos para la creación de ejercicios:} Para cumplir este requisito, debemos elegir una forma
de guardar nuestros datos para luego poder manejarlos a nuestro gusto, y por ello tenemos que conocer el funcionamiento de alguna 
herramienta que nos ayude a cumplir este objetivo.
\item \textbf{Implementación de algoritmo para controlar los ejercicios:} Para este objetivo tenemos que analizar datos que hemos recogido
para poder implementar un algoritmo que controle ejercicios, y necesitamos conocer además de los datos, las herramientas para poder
desarrollarlo correctamente.
\item \textbf{Desarrollo de una \acs{NUI} para la aplicación:} Para cumplir este objetivo debemos estudiar los elementos que posee la 
librería y adquirir las habilidades técnicas necesarias para realizar la interfaz de nuestra aplicación.
\item \textbf{Análisis, interpretación, manejo y representación de los datos obtenidos:} Para poder completar este objetivo y realizar este módulo
tenemos que conocer alguna herramienta que nos facilite representar y poder manejar los resultados de los ejercicios de manera sencilla.
\item \textbf{Integración de todos los módulos de la aplicación:} Tenemos que realizar todo tipo de pruebas para integrar los módulos
y que el sistema funcione correctamente.
\end{itemize}

Ahora exponemos cada iteración:

\begin{itemize}
\item \textbf{Iteración 0:} Realización del estado del arte.
\item \textbf{Iteración 1:} Introducción al desarrollo con Kinect.
\item \textbf{Iteración 2:} Implementación de la captura y grabación de datos del esqueleto.
\item \textbf{Iteración 3:} Realización de un control visual de datos recogidos.
\item \textbf{Iteración 4:} Implementación del algoritmo de control de ejercicios.
\item \textbf{Iteración 5:} Implementación de una interfaz gráfica para la aplicación.
\item \textbf{Iteración 6:} Realización de gráficas para la representación de los datos.
\end{itemize}

\subsubsection{Development}
Esta fase está dividida en 7 iteraciones como vimos en el apartado anterior, y como objetivo en este apartado se enumerarán las tareas
que se van a llevar a cabo en cada una de las iteraciones.

\paragraph{Iteración 0 \newline}

Esta primera iteración tiene como objetivo recoger toda la información y analizarla para poder comenzar con el proyecto. Así, el
Sprint Backlog de esta iteración comprende los siguientes elementos de alto nivel del Product Backlog: \textit{``Análisis del estado del arte'' y 
``Análisis de librería para el desarrollo''}. A continuación se presenta el Sprint Backlog con las tareas correspondientes de bajo nivel:

\begin{itemize}
 \item Recoger información general sobre es la \acs{AR}.
 \item Analizar la problemática de la enfermedad y estudiar diversos parámetros de la misma.
 \item Analizar el tratamiento para la enfermedad.
 \item Analizar la importancia del ejercicio como tratamiento de la \acs{AR}.
 \item Recoger información general sobre Kinect.
 \item Estudiar los dispositivos Kinect y realizar una comparativa de las versiones que existen.
 \item Estudiar el funcionamiento de Kinect y las posibilidades que ofrece.
 \item Analizar las distintas librerías de desarrollo que tiene Kinect mostrando sus ventajas y desventajas.
 \item Recoger información general sobre las \acs{NUI} y analizar los dispositivos que usan este tipo de interfaz.
 \item Recoger información general sobre las bases de datos \acs{XML}.
 \item Analizar las características de las bases de datos \acs{XML}.
\end{itemize}

\paragraph{Iteración 1 \newline}
El objetivo en esta segunda iteración es en un primer momento la selección de la librería para el desarrollo, y después
investigar todo lo que nos ofrece la misma para obtener conocimientos y herramientas que apoyen la implementación de 
la aplicación. Se llevaran a cabo las siguientes tareas del Product Backlog: \textit{``Elección de librería para el desarrollo'' y
``Identificación y análisis de los datos recogidos por el dispositivo''}. La división de tareas a bajo nivel para definir el Sprint Backlog
son las siguientes:
\begin{itemize}
 \item Realización de ejemplos con las distintas librerías.
 \item Análisis del uso de las librerías y elección de la que se utilizará.
 \item Estudio de la librería seleccionada.
 \item Instalación de la librería seleccionada y preparación del entorno.
 \item Estudio y aprendizaje de los recursos que ofrece la librería.
\end{itemize}

\paragraph{Iteración 2 \newline}
En esta tercera iteración se intentará indagar en las librerías para acceder a todas las características del dispositivo y poder 
realizar una captura y grabación de datos. Se terminará la tarea de \textit{``Identificación y análisis de los datos recogidos por
el dispositivo''} la cual era tan grande que la dividimos entre las dos iteraciones. El Sprint Backlog definido es el siguiente:
\begin{itemize}
 \item Acceso a Kinect y a todos sus flujos de datos.
 \item Acceso al esqueleto y aprendizaje de cómo devuelve los datos Kinect.
 \item Manejo de distintos elementos visuales.
 \item Manejo de otros elementos que ofrece Kinect.
 \item Realizar estructura que guarde los datos del esqueleto.
 \item Grabación y captura de los ejercicios realizados por un profesional.

\end{itemize}

\paragraph{Iteración 3 \newline}
Para llevar a cabo el objetivo de esta iteración,se necesita visualizar los datos para tener una idea del control del movimiento. En 
esta iteración se cumplirá la tarea de \textit{``Análisis de los datos recogidos para la creación de ejercicios''}. La división de tareas a 
bajo nivel para definir el Sprint Backlog son las siguientes:
\begin{itemize}
 \item Almacenamiento de los datos grabados.
 \item Acceso a los datos capturados.
 \item Representación visual de los datos capturados.
 \item Estudio de las representaciones de los datos de los ejercicios.
 \item Planteamiento del algoritmo después de la visualización de los datos.
 \item Preparar todas las herramientas para la realización del algoritmo de control de ejercicios. 
\end{itemize}

\paragraph{Iteración 4 \newline}
El objetivo de esta iteración es que a partir del análisis de los datos recogidos, se pueda realizar un algoritmo el cual pueda controlar
los ejercicios a los distintos usuarios que usen la aplicación. Para terminar esta iteración se utiliza parte de la siguiente porque conlleva
más tiempo del que pueda durar la iteración por lo que se intenta cumplir la tarea de \textit{``Implementación de algoritmo para controlar 
los ejercicios''} y se divide en estas tareas de bajo nivel.
\begin{itemize}

 \item Planteamiento del método de la calibración.
 \item Planteamiento de ejercicios a realizar.
 \item Cálculo de mediciones en la calibración y elección de articulaciones implicadas en el ejercicio.
 \item Adaptación del ejercicio al usuario.
 \item Controlar el área de realización del ejercicio.
 \item Realizar método de control del ejercicio.
 \item Ayudar al usuario a visualizar su ejercicio.
 \item Controlar las repeticiones que realiza el usuario.
 \item Ayudar al usuario a realizar bien el ejercicio.

\end{itemize}

\paragraph{Iteración 5 \newline}
En esta iteración se desea crear una interfaz para una presentación más atractiva de los ejercicios y también se realizará la integración
de los ejercicios en la interfaz y ajustar algunos detalles del algoritmo. Con esta iteración se cumplen las tareas de \textit{``Desarrollo
de una \acs{NUI} para la aplicación''} y también terminar la tarea de \textit{``Implementación de algoritmo para controlar los ejercicios''}.
Las tareas de bajo nivel del Sprint Backlog son:
\begin{itemize}
 \item Estudio de controles para \acs{NUI} en Kinect.
 \item Creación del logotipo.
 \item Realizar estructura de la interfaz.
 \item Integrar ejercicios en interfaz.
 \item Controlar excepciones según características de usuario en el ejercicio.
 \item Cálculo de máximos en ejercicios.
 
\end{itemize}

\paragraph{Iteración 6 \newline}
El objetivo de la última iteración es realizar una sencilla visualización de datos dentro de la aplicación y terminar el desarrollo de la 
aplicación. Cuando finalicemos la iteración se habrán terminado las dos últimas tareas del Product Backlog que son \textit{``Análisis, 
interpretación, manejo y representación de los datos obtenidos'' y ``Integración de todos los módulos de la aplicación''}. La división de
tareas de bajo nivel para esta iteración en el Sprint Backlog son:
\begin{itemize}
 \item Crear estructura con la base de datos.
 \item Acceder con .Net a la base de datos.
 \item Guardado de datos en la base de datos.
 \item Consulta de datos en la base de datos.
 \item Visualización gráfica de los datos.
 \item Integración de la visualización de los datos en la aplicación.
\end{itemize}


\subsubsection{Post-Game}
El principal objetivo de esta fase consiste en la realización de las pruebas pertinentes para comprobar el correcto funcionamiento e 
integración de los módulos implementados en la fase de desarrollo y el respectivo análisis de los resultados obtenidos. Además,
también se lleva a cabo la finalización de la documentación generada a lo largo del proyecto.

Por último, es realizado el proceso de revisión final del proyecto y se prepara el producto obtenido para su posterior presentación al
cliente o usuario final. En este caso, cómo se trata de un proyecto académico este rol será interpretado por el correspondiente tribunal
académico.

%\subsection{Estimación del esfuerzo}
%\label{estimacion_esfuerzo}


\section{Herramientas}
\label{herramientas}
En este apartado mostraremos las herramientas utilizadas para el desarrollo de la aplicación.

\subsection{Aplicaciones utilizadas}
\label{aplicaciones}
\begin{description}
 \item[Visual Studio 2013:] Es un entorno de desarrollo para sistemas operativos Windows. Soporta
 diversos lenguajes de programacion como C++, C$\sharp$, VisualBasic.NET, Java, Python entre otros.
 \item[SDK for Windows Kinect 1.8:] Es un kit de desarrollo de software de Microsoft para el dispositivo
 Kinect que contiene archivos de cabecera, bibliotecas, ejemplos, documentación y herramientas para el desarrollo
 de aplicaciones para Kinect.
 \item[.NET Framework 4.5:] Marco de software desarrollado por Microsoft que se ejecuta principalmente
 en Microsoft Windows.
 \item[Microsoft Excel 2013:] Aplicación desarrollada y distribuida por Microsoft para hojas de cálculo.
 \item[Intellij IDEA 14.1.3:] En un entorno de desarrollo integrado para el desarrollo de programas informáticos
 desarrollado por JetBrains.
 \item[eXistDB:] Es una base de datos \acs{NoSQL} de código abierto bajo licencia \acs{LGPL} construida en la
 tecnología \acs{XML}
 \item[Mercurial:] Sistema de control de versiones distribuido. Se ha hecho uso de esta herramienta
para el código del proyecto y la documentación.
 \item[Kile:] Es un editor de \TeX{}/\LaTeX{}. Funciona conjuntamente con KDE en varios sistemas operativos.
 \item[Gimp:] Programa libre y gratuito para la edición de imágenes digitales en forma de mapa de bits, tanto 
 dibujos como fotografías.
 \item[Dia:]  Aplicación para la creación de diagramas, desarrollada como parte del proyecto \acf{GNOME}.
 \item[GoConqr:] Herramienta online para la creación de mapas mentales.

 
\end{description} 
\subsection{Lenguajes de programación}
\label{lenguajes}
\begin{description}
 \item[C$\sharp$:] Lenguaje de programación orientado a objetos desarrollado y estandarizado por Microsoft 
 como parte de su plataforma .NET.
 \item[Java:] Lenguaje de programación de propósito general, concurrente, orientado a objetos que fue diseñado
 específicamente para tener tan pocas dependencias de implementación como fuera posible.
  \item[\LaTeX{}:] Lenguaje de marcado de documentos de carácter técnico y científica, utilizado para
realizar este documento.
\end{description} 

\subsection{Hardware}
\label{hardware}
\begin{description}
 \item[Ordenador portátil:] Para el desarrollo del proyecto se ha utilizado un portátil marca ASUS modelo A52J,
 procesador 4x Intel(R) Core(TM) i5 CPU a 2.53 GHz y memoria 4GB. Tres particiones.
 \item[Ordenador sobremesa:] En principio se utilizó el ordenador de sobremesa con estas características Intel(R) 
 Core(TM) i7-2620M CPU @ 2.70GHz, 4 GB de memoria y 500 GB Disco Duro, para realizar pruebas con Kinect.
 \item[Kinect para Xbox360:] Es un ``controlador de juego libre y entretenimiento'' del que se ha hablado amplimente
 en el capítulo de antecedentes.
\end{description} 
\subsection{Sistemas Operativos}
\label{hardware}
\begin{description}
 \item[Windows 7:] Es una versión de Microsoft Windows, línea de sistemas operativos producida por Microsoft Corporation.
 Instalado en una partición del ordenador portátil descrito en~\ref{hardware}. Se usa para el desarrollo de la aplicación.
 \item[Ubuntu:] Se ha empleado Ubuntu 14.04 LTS, instalado en una partición del ordenador portátil descrito en~\ref{hardware},
 y sobre todo se ha utilizado para la documentación.
\end{description} 